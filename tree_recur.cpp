#include <iostream>
#include <string>
#include <sstream>
using namespace std;
const int LEN = 10000;
const int INF = 100000000;
typedef int node;

int midOrder[LEN];
int postOrder[LEN];

node Trav(int s1, int e1, int s2, int e2, int& v)
{
	int root = postOrder[e2];
	v += root;

	int p = s1;
	bool hasChildren = false;
	while(midOrder[p] != root && p < e1)	p++;
	int llen = p - s1;
	int rlen = e1 - p;

	int vl = INF;
	int vr = INF;
	node nl, nr;
	if (llen > 0) {	// lch
		vl = 0;
		nl = Trav(s1, p - 1, s2, s2 + llen - 1, vl);
		hasChildren = true;
	}
	if (rlen > 0) {	// rch
		vr = 0;
		nr = Trav(p + 1, e1, s2 + llen, e2 - 1, vr);
		hasChildren = true;
	}
	if (!hasChildren)
		return root;
	if (vl < vr) {
		v += vl;
		return nl;
	}
	else if (vl > vr) {
		v += vr;
		return nr;
	}
	else {
		v += vl;
		return (nl < nr) ? nl : nr;
	}
}

int main()
{
	string str;
	int len, minVal, minLeaf;
		
	while (getline(cin, str)) {
		int i = 0;
		int j = 0;
		stringstream sstr(str);
		while (sstr >> i)
			midOrder[j++] = i;
		
		getline(cin, str);
		j = 0;
		sstr.str(str);
		sstr.clear();
		while (sstr >> i)
			postOrder[j++] = i;
		len = j;

		minVal = 0;
		minLeaf =  Trav(0, len - 1, 0, len - 1, minVal);

		cout << minLeaf << endl;
	}
}