#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>
#define LEN 1000000
using namespace std;
typedef unsigned long long ui64;
typedef long long i64;
typedef unsigned int ui;

ui64 person[LEN];
i64 C[LEN];

inline ui64 abs(i64& in)
{
	return (in < 0) ? -in : in;
}

int main()
{
	ui N;
	i64 x;
	ui64 average, total;
	
	while(scanf("%d", &N) == 1) {
		
		total = 0;
		for (int i = 0; i < N; i++) {
			scanf("%d", &person[i]);
			total += person[i];
		}
		average = total / N; 				// total is guaranteed to be divisible by N
		
		C[0] = 0;
		total = 0;
		for (int i = 0; i < N - 1; i++) {
			total += person[i];
			C[i+1] = total - (i + 1) * average;
		}
		
		sort(C, C+N);
		x = (N % 2) ? C[(N-1)/2] : (C[N/2-1] + C[N/2]) / 2;
		
		total = 0;
		for (int i = 0; i < N; i++)
			total += abs(x - C[i]);
		
		printf("%ld\n", total);
	}
	return 0;
}