#include <iostream>
#include <algorithm>
#include <string>
#include <stack>
#include <vector>
using namespace std;

struct set
{
	vector<int> num;
	vector<set> nestedSet;
public:
	set() {}
	set(const set& inSet) :
		num(inSet.num),
		nestedSet(inSet.nestedSet) {}
};

stack<set> stk;

inline bool SameNum(const set& s1, const set& s2)
{
	for (int i = 0; i < s1.num.size(); i++)
		if (find(s2.num.begin(), s2.num.end(), s1.num[i]) == s2.num.end())
			return false;
	return true;
}


bool SameSet(const set& s1, set s2)
{
	if ((s1.num.size() != s2.num.size()) ||
		(s1.nestedSet.size() != s2.nestedSet.size()) ||
		!SameNum(s1, s2))
		return false;
	// check nested sets
	for (int i = 0; i < s1.nestedSet.size(); i++) {
		set nt1(s1.nestedSet[i]);

		int j = 0;
		for (; j < s2.nestedSet.size(); j++)
			if (SameSet(nt1, s2.nestedSet[j]))
				break;
		if (j == s2.nestedSet.size())
			return false;
		s2.nestedSet.erase(s2.nestedSet.begin() + j);
	}

	return true;
}

void Push()
{
	set st1;
	stk.push(st1);
}

void Dup()
{
	set st1(stk.top());
	stk.push(st1);
}

void Union()
{
	set poped(stk.top());
	stk.pop();
	set& topmost = stk.top();

	int i, j;
	for (i = 0; i < poped.num.size(); i++) {
		int t = poped.num[i];
		if (find(topmost.num.begin(), topmost.num.end(), t) == topmost.num.end())
			topmost.num.push_back(t);
	}

	int len = topmost.nestedSet.size();
	for (i = 0; i < poped.nestedSet.size(); i++) {
		set st1 = poped.nestedSet[i];

		for (j = 0; j < len; j++)
			if (SameSet(st1, topmost.nestedSet[j]))
				break;
		if (j == len)
			topmost.nestedSet.push_back(st1);
	}
}

void Intersect()
{
	set st1(stk.top());
	stk.pop();
	set st2(stk.top());
	stk.pop();

	set st3;
	int i, j;
	for (i = 0; i < st1.num.size(); i++) {
		int t = st1.num[i];
		if (find(st2.num.begin(), st2.num.end(), t) != st2.num.end())
			st3.num.push_back(t);
	}

	int st1s = st1.nestedSet.size();
	int st2s = st2.nestedSet.size();
	if (st1s > st2s) {
		for (i = 0; i < st2s; i++) {
			for (j = 0; j < st1s; j++)
				if (SameSet(st2.nestedSet[i], st1.nestedSet[j]))
					break;
			if (j != st1s)
				st3.nestedSet.push_back(st2.nestedSet[i]);
		}
	}

	else {
		for (i = 0; i < st1s; i++) {
			for (j = 0; j < st2s; j++)
				if (SameSet(st1.nestedSet[i], st2.nestedSet[j]))
					break;
			if (j != st2s)
				st3.nestedSet.push_back(st1.nestedSet[i]);
		}
	}

	stk.push(st3);
}

void Add()
{
	set st1(stk.top());
	stk.pop();
	set& topmost = stk.top();
	if (st1.nestedSet.empty() && (st1.num.size() <= 1)) {
		int t;
		
		if (!st1.num.empty())
			t = st1.num[0] + 1;
		else
			t = 1;
		
		if (find(topmost.num.begin(), topmost.num.end(), t) == topmost.num.end())
				topmost.num.push_back(t);
	}
	else{
		int i = 0;
		for ( ; i < topmost.nestedSet.size(); i++)
			if (SameSet(topmost.nestedSet[i], st1))
				break;
		if (i == topmost.nestedSet.size())
			topmost.nestedSet.push_back(st1);
	}
}

int main()
{
	int times, ops;
	cin >> times;
	string str;
	for (int i = 0; i < times; i++) {
		cin >> ops;
		
		while ((ops-- > 0) && (cin >> str)) {
			if (str == "PUSH")
				Push();
			else if (str == "DUP")
				Dup();
			else if (str == "UNION")
				Union();
			else if (str == "INTERSECT")
				Intersect();
			else if (str == "ADD")
				Add();
			cout << stk.top().num.size() + stk.top().nestedSet.size() << endl;
		}
		cout << "***\n";
		stack<set>().swap(stk);
	}
}