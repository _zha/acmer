#include <cstdio>
#include <queue>
#define MAX_NODES 511
using namespace std;
struct node {
	node(): v(-1), left(NULL), right(NULL) {}
	int v;
	node* left;
	node* right;
};
node* all_nodes[MAX_NODES];
int print_val [MAX_NODES];

node* CreateBT(int depth)
{
	node* root = new node();
	queue<node*> q;
	q.push(root);

	int n_node = 1;
	for (int i = 1; i < depth; i++) {
		for (int j = 0; j < n_node; j++) {
			node* l = new node();
			node* r = new node();
			q.front()->left = l;
			q.front()->right = r;
			q.pop();
			q.push(l);
			q.push(r);
		}
		n_node *= 2;
	}
	// release queue
	return root;
}

void Print(node* root, int nodes, bool failed)
{
	if (failed) {
		printf("not complete\n");
		return;
	}

	queue<node*> q;
	q.push(root);
	
	int val;
	int n_node = 0;

	node* qf;
	do {
		qf = q.front();
		val = qf->v;
		if (val != -1) {
			print_val[n_node] = val;
			n_node++;

			if ((qf->left) && (qf->left->v != -1))
				q.push(qf->left);
			if ((qf->right) && (qf->right->v != -1))
				q.push(qf->right);
		}
		q.pop();

	} while (!q.empty());

	if (n_node == nodes) {
		for (int i = 0; i < nodes; i++) 
				printf("%d ", print_val[i]);
		putchar('\n');
	}
	else
		printf("not complete\n");
	
}

inline void Clean(int nodes)
{
	for (int i = 0; i < nodes; i++)
		all_nodes[i]->v = -1;
}

void ReleaseBT(node* t)
{
	if (!t)
		return;
	
	if (t->left != NULL)
		ReleaseBT(t->left);
	if (t->right != NULL)
		ReleaseBT(t->right);

	delete t;
}

int main()
{
	// a tree with depth 9
	node* root = CreateBT(9);

	char ch;
	node* cur = root;
	bool fail_status = false;
	int val, nodes = 0;
	while((ch = getchar()) != EOF) {
		switch (ch) {
			case '(':
				cur = root;			// move to original position
				if (scanf("%d", &val) != 1) {
					getchar();		// chew in ')'
					Print(root, nodes, fail_status);
					Clean(nodes);
					nodes = 0;
					fail_status = false;
				}
				break;
			case 'L':
				cur = cur->left;
				break;
			case 'R':
				cur = cur->right;
				break;
			case ')':
				if (cur->v == -1) {
					cur->v = val;
					all_nodes[nodes++] = cur;
				}
				else
					fail_status = true;
			default:
				break;
		}
	}

	ReleaseBT(root);
	return 0;
}