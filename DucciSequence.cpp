/**
 * 若 n-tuple 的 n 为 PowerOf2 则一定ZERO（见 wikipedia）
 * dumb mistakes: head(forgot that nums[0] is already changed), zeroCnt(forgot to clear it in next new loop)
 */


#include <cfloat>
#include <cmath>
#include <vector>
#include <iostream>
using namespace std;

const int LOOP_TIMES = 1001;

inline bool IsPowerof2(int m)
{
	double x = log(m) / log(2);
	return (x - int(x)) <= DBL_EPSILON;
}

int main()
{
	int times, N;
	vector<int> nums;
	cin >> times;
	while(times--) {
		nums.clear();

		cin >> N;
		if (IsPowerof2(N)) {
			for (int i = 0; N--; cin >> i);
			cout << "ZERO\n";
			continue;
		}
		else {
			int t;
			while(N--) {
				cin >> t;
				nums.push_back(t);
			}

			int head, diff, zeroCnt;
			int i = LOOP_TIMES;
			for ( ; i > 0; i--) {
				head = nums[0];
				zeroCnt = 0;
				for (int j = 0; j < nums.size(); j++) {
					if ((j + 1) == nums.size())
						diff = nums[j] - head;
					else
						diff = nums[j] - nums[j + 1];

					nums[j] = abs(diff);
					if (!nums[j])
						zeroCnt++;
				}
				if (zeroCnt == nums.size())
					break;
			}

			if (i)
				cout << "ZERO\n";
			else
				cout << "LOOP\n";
		}
	}
}