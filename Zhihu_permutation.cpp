#include <vector>
#include <algorithm>
#include <map>
#include <iostream>
#include <cfloat>
#include <set>
using namespace std;
typedef set<int> mtuples;
int cnt = 0;
map<vector<mtuples>, int> id;
inline bool equ(double x, double y)
{
	return ((x - y) <= DBL_EPSILON) && (-DBL_EPSILON <= (x - y));
}

inline bool IDexisted(const vector<mtuples>& t)
{
	if (id.count(t))
		return true;
	id[t] = cnt++;
	return false;
}

int main()
{
	int num[10];
	for (int i = 0; i < 10; i++)
		num[i] = i + 1;

	
	int n_ans = 0;
	
	set<int> all;
	set<int> a1, a2, a3;
	do {
		int flag = 0;
		mtuples t1 (num, num + 3);
		mtuples t2 (num + 3, num + 6);
		mtuples t3 (num + 6, num + 8);
		vector<mtuples> smt = {t1, t2, t3};
		if (!IDexisted(smt))
			flag = 1;

		if (!flag)
			continue;
		else {
			double mexp1 = (double)(num[0] + num[1] + num[2]) / (num[3] + num[4] + num[5]);
			double mexp2 = (double)(num[6] + num[7]) / (num[8] + num[9]);
			if (equ(mexp1, mexp2)) {
				n_ans++;/*
				for (int i = 0; i < 10; i++)
					cout << num[i] << " ";
				cout << endl;*/
			}
		}

	} while (next_permutation(num, num + 10));

	cout << n_ans << endl;
}