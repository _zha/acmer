/** Some may overflow (Even 3*LEN + 1 is far too small) so you just need to record nums between 0 and LEN+1 
  * a isn't supposed to be smaller than b
  * AND a and b should be output in the same order in which they appeared in the input
  */

#include <cstdio>
using namespace std;

struct RecElem
{
	RecElem() : key(0), val(0) {}
	int key;
	int val;
};

const int LEN = 1000000;
int ht[LEN + 1];
RecElem rec[LEN];

int main()
{
	int s, e, a, b, maxc;
	ht[1] = 1;
	while(scanf("%d %d", &a, &b) == 2) {
		int maxc = 0;
		s = (a < b) ? a : b;
		e = (a < b) ? b : a;
		for (int m = s; m <= e; m++) {
			int x = m;
			int t = 0;
			int step = 0;
			while(x != 1) {
				if (x < LEN) {
					if (ht[x])
						break;
					rec[t].key = x;
					rec[t].val = step;
					t++;
				}
				step++;
				if(x % 2)
					x = 3 * x + 1;
				else
					x = x / 2;
			}
			
			step += ht[x];

			if (maxc < step)
				maxc = step;
			for (int j = 0; j < t; j++) {
				ht[rec[j].key]= step - rec[j].val;
			}
		}
		printf("%d %d %d\n", a, b, maxc);
	}
	return 0;
}